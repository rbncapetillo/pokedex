import React from 'react'
import { ActivityIndicator, FlatList, Image, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { styles } from '../theme/appTheme';
import { usePokemonPages } from '../hooks/usePokemonPages';
import { PokemonBio } from '../components/PokemonBio';

export const HomeScreen = () => {
  const { top } = useSafeAreaInsets();
  const { normalPokemon, getPokemons } = usePokemonPages();
  return (
    <>
    <Image 
      source={require('../assets/pokeball.png')}
      style={styles.wallpaperPokeball}
    />
    <View
      style={{
        alignItems: 'center'
      }}
    >
      <FlatList 
        data={ normalPokemon }
        keyExtractor={(pokemon) => pokemon.id }
        showsVerticalScrollIndicator={false}
        numColumns={2}
        ListHeaderComponent={(
          <Text
          style={{
            ...styles.title,
            ...styles.globalMargin,
            top: top + 30,
            marginBottom: top + 45,
            paddingBottom: 10
          }}
        >POKEDEX 2000</Text>
        )}
        renderItem={({item}) => (
          <PokemonBio 
            pokemon={item}
          />
        )}
        onEndReached={ getPokemons }
        onEndReachedThreshold={0.3}
        ListFooterComponent={(
          <ActivityIndicator 
            style={{height: 100}}
            size={ 20 }
            color='grey'
          />
        )}
      />
    </View>
    </>
  )
}
