import { StackScreenProps } from '@react-navigation/stack'
import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { RootStackParams } from '../navigatior/Navigator'
import Icon from 'react-native-vector-icons/Ionicons';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Image } from 'react-native';
import { FadeInImage } from '../components/FadeInImage';
import { usePokemon } from '../hooks/usePokemon';
import { PokemonRx } from '../components/PokemonRx';

interface Props extends StackScreenProps<RootStackParams, 'PokemonScreen'> {};

export const PokemonScreen = ({navigation, route}: Props) => {

  const { normalPokemon, color } = route.params;
  const { top } = useSafeAreaInsets();
  const { id, name, profilePic } = normalPokemon;
  const { isLoading, pokemonRaw } = usePokemon(id);
  
  
  return (
    <View style={{ flex: 1 }}>
      <View style={{ 
        ...styles.pokemonContainer,
        backgroundColor: color
      }}>
        <TouchableOpacity
        onPress={() => navigation.pop()}
          activeOpacity={0.6}
          style={{
            ...styles.buttonReturn,
            top: top + 15
          }}
        >
          <Icon 
            name="arrow-back-outline"
            color='white'
            size={ 50 }
          />
        </TouchableOpacity>
        <Text
          style={{
            ...styles.namePokemon,
            top: top + 50
          }}
        >
          { name + '\n' } #{id}
        </Text>
        <Image 
          source={require('../assets/pokeball.png')}
          style={styles.pokeball}
        />
        <FadeInImage
          uri={ profilePic }
          style={ styles.profilePokemon }
        />
      </View>

      {
        isLoading ?
        (
          <View style={styles.loader}>
          <ActivityIndicator 
            color={ color }
            size={ 50 }
          />
        </View>
        )
        :
        <PokemonRx
          pokemon={ pokemonRaw }
        />
      }
    </View>
  )
}

const styles = StyleSheet.create({
  pokemonContainer: {
    zIndex: 999,
    height: 400,
    alignItems: 'center',
    borderBottomRightRadius: 1000,
    borderBottomLeftRadius: 1000
  },
  buttonReturn: {
    position: 'absolute',
    left: 20
  },
  namePokemon: {
    top: 20,
    color: 'white',
    fontSize: 40,
    alignSelf: 'flex-start'
  },
  pokeball: {
    width: 250,
    height: 250,
    top: 70,
    opacity: 0.2
  },
  profilePokemon: {
    width: 250,
    height: 250,
    position: 'absolute',
    top: 90
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
