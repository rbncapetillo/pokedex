import { useEffect, useRef, useState } from "react";
import { pokeApi } from '../api/pokeApi';
import { PagesPokemon, NormalPokemon, Result } from '../guis/pokeGuis';

export const usePokemonPages = () => {

  const [isLoading, setIsLoading] = useState(true)
  const [normalPokemon, setNormalPokemon] = useState<NormalPokemon[]>([])
  const nextPageUrl = useRef('https://pokeapi.co/api/v2/pokemon?limit=40');

  const getPokemons = async() => {
    setIsLoading(true);
    const response = await pokeApi.get<PagesPokemon>(nextPageUrl.current);
    nextPageUrl.current = response.data.next;
    mapPokemonList(response.data.results);
  }

  const mapPokemonList = (pokemonCollection: Result[]) => {

    const newPokemonCollection = pokemonCollection.map(({name, url}) => {
      const urlSections = url.split('/');
      const id = urlSections[urlSections.length - 2];
      const profilePic = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;

      return {
        id,
        profilePic,
        name
      }

    });

    setNormalPokemon([...normalPokemon, ...newPokemonCollection]);
    setIsLoading(false);
    
  }

  useEffect(() => {
    getPokemons();
  }, [])

  return {
    isLoading,
    normalPokemon,
    getPokemons
  }
}