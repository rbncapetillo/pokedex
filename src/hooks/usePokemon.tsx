import { useEffect, useState } from 'react'
import { PokemonData } from '../guis/pokeGuis';
import { pokeApi } from '../api/pokeApi';

export const usePokemon = ( id: string ) => {
  const [isLoading, setisLoading] = useState(true)
  const [pokemonRaw, setPokemonRaw] = useState<PokemonData>({} as PokemonData)
  const getPokemonData = async() => {
    const response = await pokeApi.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
    setPokemonRaw(response.data);
    setisLoading(false);
  }
  useEffect(() => {
    getPokemonData()
  }, [])
  

  return {
    isLoading,
    pokemonRaw
  }
}
