import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  globalMargin: {
    marginHorizontal: 20
  },
  wallpaperPokeball: {
    width: 100,
    height: 100,
    position: 'absolute',
    top: -5,
    right: 0,
    opacity: 0.3
  },
  title: {
    fontSize: 35,
    fontWeight: 'bold'
  }
})