import React from 'react'
import { View, Text, StyleSheet, StatusBarIOS } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { PokemonData } from '../guis/pokeGuis';
import { FadeInImage } from './FadeInImage';

interface Props {
  pokemon: PokemonData;
}

export const PokemonRx = ({pokemon}: Props) => {
  return (
    <ScrollView
      style={{
        ...StyleSheet.absoluteFillObject,
      }}
      showsVerticalScrollIndicator={false}
    >
      <View style={{
        ...styles.container,
        marginTop: 400
      }}>
        <Text style={styles.title}>Tipos</Text>
        <View style={{ flexDirection: 'row' }}>
          {
            pokemon.types.map(({type}) => (
              <Text 
                style={{
                  ...styles.normalText,
                  marginRight: 10
                }} 
                key={ type.name }
              >
                { type.name }
              </Text>
            ))
          }
        </View>
        <Text style={styles.title}>Peso</Text>
        <Text style={styles.normalText}> { pokemon.weight } LB </Text>
      </View>

      <View style={{
        ...styles.container,
        marginTop: 20
      }}>
        <Text style={styles.title}>Avistamientos</Text>
      </View>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      >
        <FadeInImage 
          uri={ pokemon.sprites.front_default }
          style={ styles.basicPic }
        />
        <FadeInImage 
          uri={ pokemon.sprites.back_shiny }
          style={ styles.basicPic }
        />
        <FadeInImage 
          uri={ pokemon.sprites.front_shiny }
          style={ styles.basicPic }
        />
      </ScrollView>
      <View style={ styles.container }>
        <Text style={styles.title}>Habilidades</Text>
        <View style={{ flexDirection: 'row' }}>
          {
            pokemon.abilities.map(({ability}) => (
              <Text 
                style={{
                  ...styles.normalText,
                  marginRight: 10
                }} 
                key={ ability.name }
              >
                { ability.name }
              </Text>
            ))
          }
        </View>
      </View>
      <View style={ styles.container }>
        <Text style={styles.title}>Ataques</Text>
        <View style={{ flexDirection:'row', flexWrap: 'wrap' }}>
          {
            pokemon.moves.map(({move}) => (
              <Text 
                style={{
                  ...styles.normalText,
                  marginRight: 10
                }} 
                key={ move.name }
              >
                { move.name }
              </Text>
            ))
          }
        </View>
      </View>
      <View style={ styles.container }>
        <Text style={styles.title}>Kardex</Text>
        <View>
          {
            pokemon.stats.map((stat) => (
              <View key={stat.stat.name} style={{flexDirection: 'row'}}>
                <Text 
                  style={{
                    ...styles.normalText,
                    marginRight: 10,
                    width: 150
                  }} 
                >
                  { stat.stat.name }
                </Text>
                <Text 
                  style={{
                    ...styles.normalText,
                    fontWeight: 'bold'
                  }} 
                >
                  { stat.base_stat }
                </Text>
              </View>
            ))
          }
        </View>
        <View style={{
          marginBottom: 15,
          alignItems: 'center'
        }}>
          <FadeInImage 
            uri={ pokemon.sprites.front_default }
            style={ styles.basicPic }
          />
        </View>
      </View>
    </ScrollView>
  )
}


const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    marginTop: 20
  },
  normalText: {
    fontSize: 20,
  },
  basicPic: {
    height: 100,
    width: 100
  }
});