import React, { useEffect, useRef, useState } from 'react'
import { TouchableOpacity, View, StyleSheet, Dimensions, Text, Image } from 'react-native';
import { NormalPokemon } from '../guis/pokeGuis';
import { FadeInImage } from './FadeInImage';
import  ImageColors  from 'react-native-image-colors';
import { useNavigation } from '@react-navigation/core';

const windowWidth = Dimensions.get('window').width

interface Props {
  pokemon: NormalPokemon
}

export const PokemonBio = ({pokemon}: Props) => {

  const [backgroundColor, setBackgroundColor] = useState('grey');
  const isReady = useRef(true);
  const navigation = useNavigation();

  useEffect(() => {
    ImageColors.getColors(pokemon.profilePic, {fallback: 'grey'})
      .then(colors => {
        if( !isReady.current ) return;
        if (colors.platform === 'android') {
          setBackgroundColor(colors.dominant || 'grey');
        } else {
          setBackgroundColor(colors.background || 'grey')
        }
      })
      return () => {
        isReady.current = false
      }
  }, []);

  return (
    <TouchableOpacity
      activeOpacity={ 0.9 }
      onPress={
        () => navigation.navigate('PokemonScreen', 
        {
          normalPokemon: pokemon,
          color: backgroundColor
        }
      )}
    >
      <View style={{
        ...styles.cardContainer,
        width: windowWidth * 0.45,
        backgroundColor: backgroundColor
      }}>
        <View>
          <Text style={ styles.name }>
            { pokemon.name }
            { '\n#' + pokemon.id }
          </Text>
        </View>
        <View style={styles.pokeballContainer}>
          <Image
            source={ require('../assets/pokeball-light.png') }
            style={styles.pokeball}
          />
        </View>
        <FadeInImage
          uri={ pokemon.profilePic }
          style={ styles.pokemon }
        />
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  cardContainer: {
    marginHorizontal: 10,
    height: 120,
    width: 160,
    marginBottom: 25,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 15,
  },
  name: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    top: 20,
    left: 10
  },
  pokeball: {
    width: 150,
    height: 150,
    position: 'absolute',
    bottom: -40,
    right: -50,
    overflow: 'hidden'
  },
  pokemon: {
    width: 120,
    height: 120,
    position: 'absolute',
    right: -5,
    bottom: -5
  },
  pokeballContainer: {
    position: 'absolute',
    width: 100,
    height: 100,
    bottom: 0,
    right: 0,
    opacity: 0.5,
    overflow: 'hidden'
  }
})
